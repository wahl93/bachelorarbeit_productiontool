# README #

### Summary ###

## Entwicklung eines Produktionstool für objektbasiertes Audio ##

Das Ziel der Bachelorarbeit war es ein prototypisches Produktionstool als Webanwendung für
objektbasiertes Audio zu entwerfen und zu implementieren. 
Objektbasiertes
Audio beschreibt die Darstellung von Audioinhalten. Ein Audioobjekt,
bestehend aus Metadaten und Audiorohdaten, beinhaltet alle nötigen Informationen,
um von einem Renderer der Wiedergabesituation entsprechend
prozessiert zu werden. Die fortlaufende Manipulation der Metadaten erlaubt
Interaktionen des Konsumenten und der Umgebung mit dem Audioobjekt.
Das in dieser Arbeit erstellte Produktionstool ermöglicht die Generierung
von Audiocontent im 360° Umfeld. Audioobjekte können mit
interaktiven Parametern versehen werden können. Das Tool verfügt über ein
graphisches 360°-Editionsumfeld und erzeugt eine Konfigurationsdatei, welche
alle für den Renderer relevanten Informationen (Presets) beinhaltet.
Zusammen mit den hinterlegten Audiodateien, welche in der Konfigurationsdatei
mit den entsprechenden Metadaten verlinkt sind, wird der Rendererprozess
gestartet.
Das Produktionstool soll als eigenständiger Service in Cloud Computing-
Systemen integrierbar sein. Hiermit bekommen Kunden die Möglichkeit komplexen
objektbasierten Content sogar auf einem schwachen Endgerät, wie
Smartphones oder Tablets, zu erstellen.
Das Tool verwendet das Webframework Django und die Containervirtualsierung Docker.

## Development of a production tool for object-based audio ##

The aim of the bachelor thesis was to implement and design a prototypical
production tool as a web application for object-based audio. 
Object-based audio describes the representation
of audio content. An audio object, consisting of metadata and
audio raw data, contains all important information to be processed by a
renderer according to the playback situation. The manipulation of the metadata
allows interactions between the consumer and the environment with
the audio object.
The production tool created in this thesis enables the generation of audio
content in a 360° environment. Audio objects shall be provided with
interactive parameters.
The tool has a graphical 360° edition environment and creates a configuration
file that contains all information (presets) relevant to the renderer.
Together with the enclosed audio files, which are linked to the corresponding
metadata in the config-file, the rendering process is started.
The tool uses the web framework Django and the virtualization software Docker.


More informations: Zusammenfassung_Bachelorarbeit_Alexandra_Wahl.pdf

### How do I get docker container running? ###

* Install docker
* Open terminal
* Command: docker build -t tool/editor .
* Command: docker run -it -p 8000:8000 tool/editor
* Open Browser and open 127.0.0.1:8000/presets


### Contact ###

* Alexandra Wahl: wahlalexandra93@gmail.com