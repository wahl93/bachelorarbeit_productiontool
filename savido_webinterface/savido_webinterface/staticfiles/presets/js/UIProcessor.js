var gui;
var Button = function(){
	this.showSG = showSG;
	this.showAO = showAO;
	this.newPV = newPV;
	this.save = write;
	this.delete = deleteObj;
};

var objPK=-1;
var aoPK=-1;

var data;
var posAttribArray=[];
var gainAttribArray=[];

var urlFile;

function showSG(){

	var sgName="";
	var sgColor=0xffffff;
	var elements=[];
	for(var i in objList){
		if (objList[i].pk == objPK){
			sgName = objList[i].fields.name;
			sgColor = objList[i].fields.color;
			sgColor = sgColor.replace("0x", "#");	
			sgColor = new THREE.Color(sgColor);
			sgColor = sgColor.getHex();
			for (var i in aoList){
				if (aoList[i].fields.obj==objPK){
					elements.push(aoList[i].fields.name);				
				}
			}
		}
	}


	var SGAttrib = function(){
		this.sgname = sgName;
		this.sgcolor = sgColor; //Add SG Rings
		this.elementList = "---";
	}

	var sgAttrib = new SGAttrib();
	var button = new Button();

	elements.push("new Audioobject");
	var name = gui.add(sgAttrib, 'sgname').name("SG Name");
	var color = gui.addColor(sgAttrib, 'sgcolor').name("SG Color");
	var sgelements = gui.add(sgAttrib, 'elementList', elements).name("Show Audioobject");

	color.onChange(function(value){
		var colorValue = new THREE.Color(value);
		for (var i in pVList){
			paramID = pVList[i].fields.parameter;
			for (var j in paramList){
				if (paramList[j].pk == paramID){
					var aoID = paramList[j].fields.audioobject;
					for (var m in aoList){
						if(aoList[m].pk == aoID){
							var objID= aoList[m].fields.obj;
							if(objID == objPK){
								var x = pVList[i].fields.valuex;
								var y = pVList[i].fields.valuez;
								var z = pVList[i].fields.valuey;
								for (var m in element){
									if (element[m].position.x == x && element[m].position.y == y && element[m].position.z == z){
										element[m].material.color = colorValue;
									} 
								}
							}
						}
					}
				}
			}
		}
	});

	sgelements.onChange(function(choice){
		if(choice == "new Audioobject")
			aoPK = -1;
		else{
			for(var i in aoList){
				if(aoList[i].fields.name == choice)
					aoPK=aoList[i].pk;			
			}
		}
		gui.destroy();
		gui=new dat.GUI();
		showAO();
	});

	var saveButton = gui.add(button, 'save').name("Save");
	
	saveButton.onChange(function(){
		data = {pk:objPK, sgAttrib, action:'writeSG'};
	});

	var deleteButton = gui.add(button, 'delete').name("Delete");

	deleteButton.onChange(function(){
		data = {action:"delete", which:"Obj", pk:objPK};
	});
}

function showAO(){
	var aoName="";
	var aoColor=0xffffff;
	var channel = 0;
  var timeRangeStart=0;
  var timeRangeStop=0;
	var whichSG ="No SG";
	var ifSG=false;
  var url ="";
	for(var i in aoList){
		if(aoList[i].pk == aoPK){
			aoName = aoList[i].fields.name;
			aoColor = aoList[i].fields.color;
			aoColor = aoColor.replace("0x", "#");	
			aoColor = new THREE.Color(aoColor);
			aoColor = aoColor.getHex();
      url = aoList[i].fields.url != null ? aoList[i].fields.url : "";
			channel = aoList[i].fields.channel != null ? aoList[i].fields.channel : 0;
      timeRangeStart = aoList[i].fields.timeRangeStart != null ? aoList[i].fields.timeRangeStart : 0;
      timeRangeStop = aoList[i].fields.timeRangeStop != null ? aoList[i].fields.timeRangeStop : 0;
			for (var j in objList){
				if(objList[j].pk == aoList[i].fields.obj){
					whichSG = objList[j].fields.name;
					ifSG = true;
					if (aoName == whichSG){
						whichSG = "No SG";
						ifSG=false;
					}
				}
			}
		}
		
			
	}
	if (aoPK == -1){
			for(var j in objList){	
				if(objList[j].pk == objPK)
					whichSG = objList[j].fields.name;
					ifSG=true;	
			}
	}

	var AOAttrib = function(){
		this.aoname=aoName;
		this.aocolor=aoColor;
		this.url = url;
		this.channel=channel;
    this.timeRangeStart=timeRangeStart;
    this.timeRangeStop=timeRangeStop;
		this.whichSG=whichSG;
	}

	var aoAttrib = new AOAttrib();
	var button = new Button();
  if (gui !== undefined)
    gui.destroy();
  gui = new dat.GUI();

	var name = gui.add(aoAttrib, 'aoname').name('Audioobject Name').setValue(aoName);
	var color = gui.addColor(aoAttrib,  'aocolor').name('AO Color');

	color.onChange(function(value){
		var colorValue = new THREE.Color(value);
		for(var i in paramList){
			if(paramList[i].fields.audioobject == aoPK){
				for(var j in pVList){
					if(pVList[j].fields.parameter == paramList[i].pk){
						var x = pVList[j].fields.valuex;
						var y = pVList[j].fields.valuez;
						var z = pVList[j].fields.valuey;
						for (var m in objects){
							if (objects[m].position.x == x && objects[m].position.y == y && objects[m].position.z == z){
								for(var k in aoList){
									if(aoList[k].pk == aoPK){
										objects[m].material.color = colorValue;
										console.log(aoList[k].fields.name);
									}								
								}
							
							} 
						}
						
					}
				}
			}
		}
	});

  var filePath = gui.add(aoAttrib, 'url').name("URL");

	gui.add(aoAttrib, 'channel', 0, 6, 1).name("Channel").setValue(channel); 	

  var timeRangeFolder = gui.addFolder("TimeRange in ms");

  timeRangeFolder.add(aoAttrib, 'timeRangeStart').name("Start");
  timeRangeFolder.add(aoAttrib, 'timeRangeStop').name("Stop");

	var posFolder = gui.addFolder("Position Values");
	var gainFolder = gui.addFolder("Gain Values");

  var x = 0;
	var y = 0;
	for(var i in pVList){
		var paramID = pVList[i].fields.parameter;
		for (var j in paramList){
			if (paramList[j].pk == paramID){
				var aoID = paramList[j].fields.audioobject;
				if(paramList[j].fields.name == "Pos" && aoID == aoPK){
					var posName = pVList[i].fields.name;
					var posNameFolder = posFolder.addFolder(posName);
          for (var index in objects){
            if (pVList[i].fields.valuex == objects[index].position.x && pVList[i].fields.valuez == objects[index].position.y && pVList[i].fields.valuey == objects[index].position.z){
					    showPos(pVList[i].pk, posNameFolder, ifSG, index, false);
               if (ifSG)
                   break;
                
              
            }
          }

          x++;
			               
				}

				if(paramList[j].fields.name == "Gain" && aoID == aoPK){
					var gainName = pVList[i].fields.name;
					var gainNameFolder = gainFolder.addFolder(gainName);
					showGain(pVList[i].pk, gainNameFolder, ifSG, y);
			    		y++;
				}
			}
		}
	}

  if(newPosName!=null){
    showPos(-1, posFolder.addFolder(newPosName), ifSG, x, true);
    x++;
  }

  var i = 1;
	var newPosButton = posFolder.add(button, "newPV").name("New Pos");
	newPosButton.onChange(function(){
		showPos(-1, posFolder.addFolder("New Position "+ [i]), ifSG, x, false);
		x++;
    i++;
	});
  
  var j = 1;
	var newGainButton = gainFolder.add(button, "newPV").name("New Gain")
	newGainButton.onChange(function(){
		showGain(-1, gainFolder.addFolder("New Gain "+ [j]), ifSG, y);
    j++;
		y++;
	});


	var switchgroups = ["No SG"];
	for (var i in objList){
		if (objList[i].fields.objectType == "Switchgroup"){
			switchgroups.push(objList[i].fields.name);
		}
	}

	var selectSG = gui.add(aoAttrib, 'whichSG', switchgroups).name("Add to SG");

	var saveButton = gui.add(button, 'save').name("Save");
	
	saveButton.onChange(function(){
		data = {pk:aoPK, aoAttrib, posAttribArray, gainAttribArray, posLength:x, gainLength:y, action:"writeAO"};
	});

	var deleteButton = gui.add(button, 'delete').name("Delete");

	deleteButton.onChange(function(){
		data = {action:"delete", which:"AO", pk:aoPK};
	});



}

function showPos(posPK, folder, ifSG, attribID, ifDBLClick){
	var posName = "";
	var posX, posY, posZ = 0;
	var forAll = false;
	for(var i in pVList){
		if (pVList[i].pk == posPK){
			posName = pVList[i].fields.name;
			posX = pVList[i].fields.valuex;
			posY = pVList[i].fields.valuez;
			posZ = pVList[i].fields.valuey;
			forAll = pVList[i].fields.forAll;
		}
	}



	var x;
	
	for (var i in objects){
		if (objects[i].position.x == posX && objects[i].position.z == posZ && objects[i].position.y == posY)
			x=i;
	}

	var PosAttrib = function(){
		this.posname="";
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.forAllAO = forAll;	
	}

	var posAttrib = new PosAttrib(); 
	var button = new Button();  




	var name = folder.add(posAttrib, 'posname').name("Position Name").setValue(posName);
	if(ifDBLClick){
    name.setValue(newPosName);
		var x = folder.add(posAttrib, 'x', -14, 14).name("X Value").setValue(newPosValue[0]);
		var z = folder.add(posAttrib, 'z', -14, 14).name("Y Value").setValue(newPosValue[2]);
		var y = folder.add(posAttrib, 'y', -14, 14).name("Z Value").setValue(newPosValue[1]);
  }
  else if (posPK==-1){
		var x = folder.add(posAttrib, 'x', -14, 14).name("X Value");
		var z = folder.add(posAttrib, 'z', -14, 14).name("Y Value");
		var y = folder.add(posAttrib, 'y', -14, 14).name("Z Value");
	}
	else{
		var x = folder.add(posAttrib, 'x', -14, 14).name("X Value").setValue(objects[attribID].position.x);
		var z = folder.add(posAttrib, 'z', -14, 14).name("Y Value").setValue(objects[attribID].position.z);//ADM Koordinaten y und z getauscht
		var y = folder.add(posAttrib, 'y', -14, 14).name("Z Value").setValue(objects[attribID].position.y);
	}

	var check = folder.add(posAttrib, 'forAllAO').name("Value is valid for all AO in SG").setValue(forAll);

	posAttribArray[attribID]={posAttrib, pk:posPK};

	name.onChange(function(){
		posAttribArray[attribID] = {posAttrib, pk:posPK};			
	});

	x.onChange(function(value){
		if(posPK!=-1)
			objects[attribID].position.x = value;
		posAttribArray[attribID] = {posAttrib, pk:posPK};
	});

	y.onChange(function(value){
		if(posPK!=-1)
			objects[attribID].position.y = value;//ADM Koordinaten y und z getauscht
		posAttribArray[attribID] = {posAttrib, pk:posPK};
	});

	z.onChange(function(value){
		if(posPK!=-1)		
			objects[attribID].position.z = value;
		posAttribArray[attribID] = {posAttrib, pk:posPK};
	});

	check.onChange(function(){
		posAttribArray[attribID] = {posAttrib, pk:posPK};
	});
	
    	
	var deleteButton = folder.add(button, 'delete').name("Delete");

	deleteButton.onChange(function(){
		data = {action:"delete", which:"PV", pk:posPK};
	});
}

function showGain(gainPK, folder, ifSG, attribID){
	var GainAttrib = function(){
		this.gainname="";
		this.value=0;
		this.forAllAO = false;
	}

	var gainAttrib = new GainAttrib();
	var button = new Button();
	
	var gainName = "";
	var gainValue = 0;
	var forAll = false;
	for(var i in pVList){
		if (pVList[i].pk == gainPK){
			gainName = pVList[i].fields.name;
			gainValue = pVList[i].fields.value;
			forAll = pVList[i].fields.forAll;

		}
	}

	var name = folder.add(gainAttrib, 'gainname').name("Gain Name").setValue(gainName);
	var value = folder.add(gainAttrib, 'value', 0, 1, 0.1).name("Gain Value").setValue(gainValue);
	var check = folder.add(gainAttrib, 'forAllAO').name("Value is valid for all AO in SG").setValue(forAll);

	gainAttribArray.push({gainAttrib, pk:gainPK});

	name.onChange(function(){
		gainAttribArray[attribID] = {gainAttrib, pk:gainPK};
	});

	value.onChange(function(){
		gainAttribArray[attribID] = {gainAttrib, pk:gainPK};
	});

	check.onChange(function(){
		gainAttribArray[attribID] = {gainAttrib, pk:gainPK};
	});
	
	var deleteButton = folder.add(button, 'delete').name("Delete");
	
	deleteButton.onChange(function(){
		data = {action:"delete", which:"PV", pk:gainPK};
	});
}

function writeObj(){
	makeAllObjectsTransparent();
	saveObject(data);
	gui.destroy();

}

function deleteObj(){
	makeAllObjectsTransparent();
	saveObject(data);
	gui.destroy();
}

function editObject(objID){
  objPK = objID;

  if (gui!==undefined)
	  gui.destroy();
  gui = new dat.GUI();

  for (var i in objList){
    if ( objList[i].pk == objPK){
      objType = objList[i].fields.objectType;
      if (objType == "Audioobject"){
        for(var i in aoList){
          if(aoList[i].fields.obj == objPK){
              aoPK = aoList[i].pk;            
              showAO();
          }
        }
      }
      else if (objType == "Switchgroup")
        showSG();

    }
  }

}

function deleteObjSidebar(objID){
	data={action:"delete", which:"Obj", pk:objID};
	saveObject(data);

}

function addNewObject(){
    if (gui)
	    gui.destroy();
    gui = new dat.GUI();
    var CreateNew = function(){
      this.choice = "addNewAO";
    }

    var createNew = new CreateNew();
    var options=[];
    
    options.push("new AudioObject");
    options.push("new SwitchGroup");
    
    var selectMenu = gui.add(createNew, 'choice', options).name("Which ObjectType");
    selectMenu.onChange(function(choice){
      gui.destroy();
      gui=new dat.GUI();
      if (choice == "new AudioObject"){
        aoPK = -1;
        showAO();	
      }
      else if (choice =="new SwitchGroup"){
        objPK = -1;
        showSG();
      }
      
    });
}

function addOutputFormat(){
  if(gui!== undefined)
    gui.destroy();
  gui = new dat.GUI();


  stereo = false;
  fivePointOne = false;
  binaural = false;

  for (var i in outputList){
    if (outputList[i].fields.name == "stereo")
      stereo = true;
    if (outputList[i].fields.name == "5.1")
      fivePointOne = true;
    if (outputList[i].fields.name == "binaural")
      binaural = true;
  }

  var OutputFormat = function(){
    this.stereo = stereo;
    this.fivePointOne = fivePointOne;
    this.binaural = binaural;
  };

  var outputFormat = new OutputFormat();
  var button = new Button();

  gui.add(outputFormat, 'stereo').name("Stereo");
  gui.add(outputFormat, 'fivePointOne').name("5.1");
  gui.add(outputFormat, 'binaural').name("Binaural");

	var saveButton = gui.add(button, 'save').name("Save");	
	saveButton.onChange(function(){
		data = {outputFormat, action:"addOutputFormat"};
	});
}

function saveScene(){
  if(gui!==undefined)
    gui.destroy();
  gui = new dat.GUI();

  var SaveScene = function(){
    this.name = "";
  }

  var saveScene = new SaveScene();
  var button = new Button();

  gui.add(saveScene, 'name').name("Scene name");
  var saveButton = gui.add(button, 'save').name("Save");

  saveButton.onChange(function(){
    data = {saveScene, action:'saveScene'};
  });
}



function newPV(){
	console.log("");
}




