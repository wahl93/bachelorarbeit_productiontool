var mouse = new THREE.Vector2();

var mousedown = false;
var mousedblclick = false;

var canvas;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(45, 1 , 1, 500);
var renderer;

var loader = new THREE.TextureLoader();

var geometryObject = new THREE.SphereGeometry(1, 32, 32);


var raycaster = new THREE.Raycaster();
var ray = new THREE.Ray();

var radius = 15;
var alpha=90;
var beta=90;



var audioobject;
var objPos;
var newPosName;
var newPosValue = [0,0,0];

var isObjectSelect = false;

  function handleMouseClick(event) {              
      mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
      mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;
            

      raycaster.setFromCamera( mouse, camera );

      var intersects = raycaster.intersectObjects( objects );       

      if ( intersects.length > 0) {

        var intersect = intersects[0];

        if (audioobject != intersect.object){
          if (audioobject != null){
            audioobject.material.transparent = true; 
          }     
                              
          audioobject = intersect.object;
          isObjectSelect = true;
          audioobject.material.transparent = false;
          var whichSG, ao;
          for (var i in pVList){
                if(pVList[i].fields.valuex == audioobject.position.x && 
                    pVList[i].fields.valuez == audioobject.position.y &&
                    pVList[i].fields.valuey == audioobject.position.z){
                  console.log("Intersect");
                  var parameter = pVList[i].fields.parameter;
                  for(var j in paramList){
                    if(paramList[j].pk == parameter){
                      aoPK=paramList[j].fields.audioobject;
                      for (var k in aoList){
                        if(aoList[k].pk == aoPK){
                          objPK=aoList[k].fields.obj;
                          showAO();
                        }
                      }
                    }
                  }
                }
          }        
        }
      }
      else{
	      makeAllObjectsTransparent();
        if(gui)
          gui.destroy();
      }
 }

 function handleDblClick(event) {
    if (gui)
      gui.destroy();
    gui = new dat.GUI();

    mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
    mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;      
    raycaster.setFromCamera( mouse, camera );

    var materialObject = new THREE.MeshBasicMaterial({transparent: false, opacity: 0.5});
    audioobject = new THREE.Mesh(geometryObject, materialObject);
   
    audioobject.position.x=raycaster.ray.direction.x;
    audioobject.position.y=raycaster.ray.direction.y;
    audioobject.position.z=raycaster.ray.direction.z;

    scene.add(audioobject);
    objects.push(audioobject);

    var PosName=function(){
      this.name="";
    };

    var posName = new PosName();

    var name=gui.add(posName, 'name').name("Position Name");
    var x = gui.add(audioobject.position, 'x', -14, 14).listen().name("X");    
    var z = gui.add(audioobject.position, 'z', -14, 14).listen().name("Y");
    var y = gui.add(audioobject.position, 'y', -14, 14).listen().name("Z");
   
    x.onChange(function(value){
      newPosValue[0]=value;
    });
    y.onChange(function(value){
      newPosValue[1]=value;
    });
    z.onChange(function(value){
      newPosValue[2]=value;
    });
        

    var CreateNew = function(){
      this.choice = "addNewAO";
    }

    var createNew = new CreateNew();
    var options=[];
    for (i in aoList)
      options.push(aoList[i].fields.name);

    options.push("new AudioObject");
    
    var selectMenu = gui.add(createNew, 'choice', options).name("New Pos in");
    selectMenu.onChange(function(choice){
      newPosName= posName['name'];
      var button = new Button();
      if (choice == "new AudioObject")
        aoPK = -1;      
      else{
        for (var i in aoList){
          if(aoList[i].fields.name == choice)
              aoPK = aoList[i].pk;                
        }
      }
      var nextButton = gui.add(button, 'showAO').name("Next");    
    });
    
 }

var distanceObj, directionX, directionY;


function handleMouseDown(event) {
  mousedown=true;
}

 function handleMouseUp(event) {
    mousedown=false;   
 }

 function handleMouseMove(event) {
    directionX, directionY = 0;

    if (mousedown == true && isObjectSelect == true){

      console.log("MouseMove");


      var oldX = audioobject.position.x;
      var oldZ = audioobject.position.z;
      var oldDistance = Math.sqrt(oldX*oldX + oldZ*oldZ);
      distanceObj = oldDistance;

      mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
      mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;

      calcPos();
    }
}

function handleMouseWheel(event){
  
    if (isObjectSelect == true){
      var oldX = audioobject.position.x;
      var oldZ = audioobject.position.z;
      var oldDistance = Math.sqrt(oldX*oldX + oldZ*oldZ);
      distanceObj = oldDistance - event.wheelDelta;

      calcPos();

  }
}

function calcPos(){

   if (audioobject && distanceObj){

     alphaObj = Math.asin(mouse.x/distanceObj);
     betaObj = Math.asin(mouse.y/distanceObj);

     audioobject.position.x = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj)) * Math.cos(degToRad(beta - 90 - betaObj));
     audioobject.position.z = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj)) * Math.sin(degToRad(beta - 90 - betaObj));
     audioobject.position.y = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj));

     console.log(distanceObj);
     console.log(audioobject.position);
   }


}

function handleKeyDown(event) {
       
    if(event.which==83)
      alpha=alpha+1;        
    if(event.which==87)
      alpha=alpha-1;
    if(event.which==68)
      beta=beta+1;
    if(event.which==65)
      beta=beta-1;
    if(event.which==107)
      audioobject.position.z +=0.5;
    if(event.which==109)
      audioobject.position.z -=0.5;



    loadScenePosition(alpha, beta);
    
  }
