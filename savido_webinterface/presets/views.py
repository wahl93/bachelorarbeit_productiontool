from django.shortcuts import render, get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError
from django.template import RequestContext
from django.views import generic
from django.core import serializers

from .models import Object, OutputFormat, AudioObject, Parameter, ParameterValue
from .DBProcessor import dbProcessor

import json
import os.path

db = dbProcessor()

# Create all views
class IndexView(generic.TemplateView):
    template_name= 'presets/index.html'
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        
        # set context with database entries for templates
        context['objList'] = Object.objects.all()
        
        data=serializers.serialize("json", OutputFormat.objects.all())
        context['outputListJS']=data        
        
        data= serializers.serialize("json", Object.objects.all())
        context['objListJS']=data
        
        data=serializers.serialize("json", AudioObject.objects.all())
        context['aoListJS']=data
        
        data=serializers.serialize("json", Parameter.objects.all())
        context['paramListJS']=data
        
        data=serializers.serialize("json", ParameterValue.objects.all())
        context['pVListJS']=data
       
        return context
        
    def get(self, request, *args, **kwargs):
        x = request.GET
        context = self.get_context_data(params=x)

        # all interactions with production tool                                                
        if 'action' in x:
            if x['action'] == "writeSG":
                db.writeSG(x)
                    
            if x['action'] == "writeAO":
                db.writeAO(x)
                
            if x['action'] == "delete":                                
                db.deleteObject(x)
                
            if x['action'] == "addOutputFormat":
                db.writeOutputFormat(x);
    
            if x['action'] == "saveScene":
                db.refreshJSON(x)

        return super(generic.TemplateView, self).render_to_response(context)
        
     
        
class PlayerView(generic.TemplateView):
    template_name='presets/player.html'
    

