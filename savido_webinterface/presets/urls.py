from django.conf.urls import url

from . import views

app_name= 'presets'

# create url patterns to open application with browser
urlpatterns = [  
    url(r'^player$', views.PlayerView.as_view(), name='player'),
    url(r'$', views.IndexView.as_view(), name='index'),
]
