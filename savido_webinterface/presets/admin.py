from django.contrib import admin

# Register your models here.

from .models import Object, AudioObject, Parameter, ParameterValue

admin.site.register(Object)
admin.site.register(AudioObject)
admin.site.register(Parameter)
admin.site.register(ParameterValue)