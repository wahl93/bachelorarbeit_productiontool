from __future__ import unicode_literals

from django.db import models


MAX_LENGTH=32
MAX_LENGTH_URL = 100

# create database tables with parameters

class OutputFormat(models.Model):
        name = models.CharField(unique=True, max_length=MAX_LENGTH)
    
            
class Object(models.Model):
    name = models.CharField(unique=True, max_length=MAX_LENGTH)
    objectType = models.CharField(unique=False, max_length=MAX_LENGTH)
    color = models.CharField(null=True, max_length=MAX_LENGTH)
    # get all audioobjects from object     
    def getObject(self, objectType):
        if objectType=="Switchgroup":
                return {'elements': self.getListOfAudioObj(),
                'type':'switchGroup'
                        }
        else:
            audioObj = AudioObject.objects.get(obj=self)
            return audioObj.getAODict()

    # get all audioobject for object/switchgroups with foreign key (obj) 
    def getListOfAudioObj(self): 
        if self.objectType == "Switchgroup":
            aoName = [audioobject.name for audioobject in AudioObject.objects.filter(obj=self).order_by('name')]      
            aoDict = [audioobject.getAODict() for audioobject in AudioObject.objects.filter(obj=self).order_by('name')] 
            # create dict from audioobjects and parameters
            listOfAO = dict(zip(aoName, aoDict))
            return listOfAO

        
class AudioObject(models.Model):
    name = models.CharField(unique=True, max_length=MAX_LENGTH)
    url = models.CharField(null = True, max_length= MAX_LENGTH_URL)
    channel = models.IntegerField(null=True, default=0)
    timeRangeStart = models.IntegerField(null=True, default=0)
    timeRangeStop = models.IntegerField(null=True, default=0)
    obj = models.ForeignKey(Object, on_delete=models.CASCADE)
    color = models.CharField(null=True, max_length=MAX_LENGTH)
    # create dict with audioobject and parameters
    def getAODict(self):
            return {'type': 'audioObject',
                    'source': { 'url' :self.url,
                               'channel' : self.channel,
                               },
                    'timeRange':{'start':self.timeRangeStart, 'stop':self.timeRangeStop},
                    'parameters': self.getListOfParam()
                    }
    # get all parameters for audioobject with foreign key (audioobject)                
    def getListOfParam(self):
        paramName = [parameter.name for parameter in Parameter.objects.filter(audioobject=self).order_by('name')]
        paramPV = [parameter.getListOfParameterValue() for parameter in Parameter.objects.filter(audioobject=self).order_by('name')]
        # create dict with parameters and pv
        listOfParam = dict(zip(paramName, paramPV))
        return listOfParam
        
class Parameter(models.Model):
    name= models.CharField(null = True, max_length= MAX_LENGTH)
    audioobject = models.ForeignKey(AudioObject, on_delete=models.CASCADE)
    # get all values for parameter with foreign key (parameter)
    def getListOfParameterValue(self):
        pvName = [parametervalue.name for parametervalue in ParameterValue.objects.filter(parameter=self).order_by('name')]
        pvValue = [parametervalue.getValue() for parametervalue in ParameterValue.objects.filter(parameter=self).order_by('name')]
        # create dict with parametervalues
        listOfPV=dict(zip(pvName, pvValue))
        return listOfPV
                

class ParameterValue(models.Model):
    name = models.CharField(null = True, max_length= MAX_LENGTH)
    value=models.FloatField(null = True, default=0)
    valuex=models.FloatField(null = True, default=0)
    valuey=models.FloatField(null = True, default=0)
    valuez=models.FloatField(null = True, default=0)
    parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
    forAll = models.BooleanField(default = False);

    # get all values for parameter with foreign key (parameter)        
    def getValue(self):
        value=self.value
        if self.parameter.name =='Pos':
            value=[self.valuex, self.valuey, self.valuez]
        return value
        
