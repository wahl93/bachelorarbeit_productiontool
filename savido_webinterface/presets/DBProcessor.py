# -*- coding: utf-8 -*-
"""
Created on Wed Apr 12 11:19:07 2017

@author: wahl

Class to process all interactions with database
"""

import json, os.path
from .models import Object, AudioObject, Parameter, ParameterValue, OutputFormat

from django.utils.datastructures import MultiValueDictKeyError

class dbProcessor(): 
    # function to write or edit an audioobject   
    def writeAO(self, newAO):
        # get all informations from newAO-Object (Ajax)
        pk = newAO['pk']
        name = newAO['aoAttrib[aoname]']
        color = hex(int(newAO['aoAttrib[aocolor]']))    
        channel = int(newAO['aoAttrib[channel]'])
        trStart = int (newAO['aoAttrib[timeRangeStart]'])
        trStop = int (newAO['aoAttrib[timeRangeStop]'])        
        whichSG = newAO['aoAttrib[whichSG]']
        url = newAO['aoAttrib[url]']
        
        # check if audioobject belongs to switchgroup 
        if whichSG =="No SG": # audioobject stands alone
            # Primary Key is -1 = Audioobject is new
            if pk =="-1":
                # write a new object with objecttype = audioobject and a name and color parameter
                obj = Object(name=name, color=color, objectType="Audioobject")
                # save new object
                obj.save()
                # write a new audioobject in database
                audioObj = AudioObject(obj=obj, name=name, color=color, channel = channel, timeRangeStart = trStart, timeRangeStop = trStop, url=url)
                # save audioObj
                audioObj.save() 
                # create pos and gain parameter (every Audioobject has pos and gain)               
                posID=audioObj.parameter_set.create(name='Pos').id
                gainID=audioObj.parameter_set.create(name='Gain').id
       
                self.writePosValues(posID, newAO)
                self.writeGainValues(gainID, newAO)
                
            else:
            # if audioobject exists, find it in DB
                try:
                    audioObj = AudioObject.objects.get(pk=pk)
                    try:
                        obj = Object.objects.get(name=audioObj.name)
                    except Object.DoesNotExist:
                        obj = Object(name=name, color=color, objectType="Audioobject")
                        obj.save()
                    # overwrite parameter from audioobjects
                    audioObj.obj = obj
                    audioObj.name = name
                    audioObj.channel = channel
                    audioObj.timeRangeStart = trStart
                    audioObj.timeRangeStop = trStop
                    audioObj.color = color
                    audioObj.url = url
                    audioObj.save()
                    posID = Parameter.objects.get(audioobject=audioObj, name="Pos").id
                    gainID = Parameter.objects.get(audioobject=audioObj, name="Gain").id
                    self.writePosValues(posID, newAO)
                    self.writeGainValues(gainID, newAO)
                except AudioObject.DoesNotExist:
                    print("Unbekannter PK")
                    
        else:
        # if audioobject belongs to a SG
            # find SG
            obj = Object.objects.get(name=whichSG)
            # same as new AO
            if pk =="-1":
                audioObj = AudioObject(obj=obj, name=name, color=color, channel = channel, timeRangeStart = trStart, timeRangeStop = trStop, url = url)
                audioObj.save()
                posID=audioObj.parameter_set.create(name='Pos').id
                gainID=audioObj.parameter_set.create(name='Gain').id    
                self.writePosValues(posID, newAO)
                self.writeGainValues(gainID, newAO)
            else:
                try:
                    audioObj = AudioObject.objects.get(pk=pk)
                    audioObj.obj=obj
                    audioObj.name = name
                    audioObj.channel = channel
                    audioObj.timeRangeStart = trStart
                    audioObj.timeRangeStop = trStop
                    audioObj.color = color
                    audioObj.url = url
                    audioObj.save()
                    posID = Parameter.objects.get(audioobject=audioObj, name="Pos").id
                    gainID = Parameter.objects.get(audioobject=audioObj, name="Gain").id
                    self.writePosValues(posID, newAO)
                    self.writeGainValues(gainID, newAO)
                except AudioObject.DoesNotExist:
                    print("Unbekannter PK")
        
    def writeSG(self, newSG):
        # function to write or edit a switchgroup
        # same as writeAO
        pk=newSG['pk']
        color = hex(int(newSG['sgAttrib[sgcolor]']))
        name = newSG['sgAttrib[sgname]']
        
        if pk=='-1':
            obj = Object(name=name, objectType="Switchgroup", color=color)
            obj.save()
        else:
            try:
                 obj = Object.objects.get(pk=pk)
                 obj.name=name
                 obj.color=color
                 obj.save()
            except Object.DoesNotExist:
                print ("Unbekannter PK")
                
    def writePosValues(self, posID, newPV):  
        # function to write or edit position values
        # same as writeAO      
        posLength = int(newPV['posLength'])
        
        for i in range(0, posLength):       
            pk = newPV['posAttribArray['+ str(i) +'][pk]']
            name = newPV['posAttribArray['+ str(i) +'][posAttrib][posname]']
            x = round(float(newPV['posAttribArray['+ str(i) +'][posAttrib][x]']),2)
            y = round(float(newPV['posAttribArray['+ str(i) +'][posAttrib][z]']),2)
            z = round(float(newPV['posAttribArray['+ str(i) +'][posAttrib][y]']),2)
            # parameters can be value for all AO in SG
            if newPV['posAttribArray['+ str(i) +'][posAttrib][forAllAO]']=="true":
                forAllAO = True
            else:
                forAllAO = False
            
            posP = Parameter.objects.get(pk = posID)
            
            if pk =="-1":
                posVal = ParameterValue(parameter = posP, name = name, valuex = x, valuey = y, valuez= z, forAll=forAllAO)
                posVal.save()
            else:
                posVal = ParameterValue.objects.get(pk = pk)
                posVal.name = name
                posVal.valuex = x
                posVal.valuey = y
                posVal.valuez = z
                posVal.forAll=forAllAO
                posVal.save()
            if forAllAO==True:
                audioobject = posP.audioobject
                obj = audioobject.obj
                allAO = AudioObject.objects.filter(obj=obj)
                for audioObj in allAO:
                    posP = Parameter.objects.get(audioobject=audioObj, name="Pos")
                    try:
                        posVal = ParameterValue.objects.get(parameter=posP, name=name)
                        posVal.valuex = x
                        posVal.valuey = y
                        posVal.valuez = z
                        posVal.forAll = forAllAO
                        posVal.save()
                    except ParameterValue.DoesNotExist:
                        posVal = ParameterValue(parameter = posP, name = name, valuex = x, valuey = y, valuez= z, forAll=forAllAO)
                        posVal.save()

        
        
        
    def writeGainValues(self, gainID, newPV):
        # function to write or edit a gain value
        # same as writeAO and writePosValues
        gainLength = int(newPV['gainLength'])
        
        for i in range(0, gainLength):
            pk = newPV['gainAttribArray['+ str(i) +'][pk]']
            name = newPV['gainAttribArray['+ str(i) +'][gainAttrib][gainname]']
            value = newPV['gainAttribArray['+ str(i) +'][gainAttrib][value]']
            
            if newPV['gainAttribArray['+ str(i) +'][gainAttrib][forAllAO]']=="true":
                forAllAO = True
            else:
                forAllAO = False
            
            gainP = Parameter.objects.get(pk = gainID)
            
            if pk =="-1":
                gainVal = ParameterValue(parameter = gainP, name = name, value= value)
                gainVal.save()
            else:
                gainVal = ParameterValue.objects.get(pk = pk)
                gainVal.name = name
                gainVal.value = value
                gainVal.save()
            if forAllAO:
                audioobject = gainP.audioobject
                obj = audioobject.obj
                allAO = AudioObject.objects.filter(obj=obj)
                for audioObj in allAO:
                    gainP = Parameter.objects.get(audioobject=audioObj, name="Gain")
                    try:
                        gainVal = ParameterValue.objects.get(parameter=gainP, name=name)
                        gainVal.value = value
                        gainVal.forAll = forAllAO
                        gainVal.save()
                    except ParameterValue.DoesNotExist:
                        gainVal = ParameterValue(parameter = gainP, name = name, value= value, forAll=forAllAO)
                        gainVal.save()

    def writeOutputFormat(self, newOF):
        # function set output format
        # same as writeAO 
        stereo = newOF['outputFormat[stereo]']
        binaural = newOF['outputFormat[binaural]']
        fivePointOne = newOF['outputFormat[fivePointOne]']
        
        try: 
            output = OutputFormat.objects.get(name="stereo")
            if stereo != 'true':
                output.delete()
        except OutputFormat.DoesNotExist:
            if stereo =='true':
                output = OutputFormat(name="stereo")
                output.save()       
                
        try: 
            output = OutputFormat.objects.get(name="5.1")
            if fivePointOne != 'true':
                output.delete()
        except OutputFormat.DoesNotExist:
            if fivePointOne =='true':
                output = OutputFormat(name="5.1")
                output.save() 
                
        try: 
            output = OutputFormat.objects.get(name="binaural")
            if binaural != 'true':
                output.delete()
        except OutputFormat.DoesNotExist:
            if binaural =='true':
                output = OutputFormat(name="binaural")
                output.save() 
                
                
    # delete an object from database with the primary key     
    def deleteObject(self, deleteObj):
        which = deleteObj['which']
        pk = deleteObj['pk']
        
        if which=='Obj':
            model = Object
        elif which =='AO':
            model = AudioObject
        elif which =='PV':
            model=ParameterValue
        else:
            return
        try:
            delObj=model.objects.get(pk=pk)
            delObj.delete()
        except model.DoesNotExist:
            print ("Element existiert nicht!!")
            
    
    # write database entries in json file
    def refreshJSON(self, saveScene):
        # find all objects
        objName =  [obj.name for obj in Object.objects.all()]
        objAO = [obj.getObject(obj.objectType) for obj in Object.objects.all()]
        listOfObj= dict(zip(objName, objAO))
        # set file name
        fileName = saveScene['saveScene[name]'] + '.json'

        # create object with all database entries for json-file
        data={'Interaktive AudioObjects':listOfObj,
              "OutputFormat":[outputFormat.name for outputFormat in OutputFormat.objects.all()]}
        # write json file
        with open(fileName, 'w') as outfile:
            json.dump(data, outfile, indent=1)

