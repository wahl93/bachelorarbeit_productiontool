from django.conf.urls import url

from . import views

app_name= 'presets'

urlpatterns = [  
    url(r'$', views.MobileView.as_view(), name='m_player')
]
