from django.shortcuts import render, get_object_or_404

from django.utils.datastructures import MultiValueDictKeyError
from django.template import RequestContext
from django.views import generic
from django.core import serializers

from .models import Object, OutputFormat, AudioObject, Parameter, ParameterValue

# Mobile view still in work

class MobileView(generic.TemplateView):
    template_name= 'mobile_presets/m_player.html'

    def get_context_data(self, **kwargs):
        context = super(MobileView, self).get_context_data(**kwargs)
        
        data=serializers.serialize("json", OutputFormat.objects.all())
        context['outputListJS']=data        
        
        data= serializers.serialize("json", Object.objects.all())
        context['objListJS']=data
        
        data=serializers.serialize("json", AudioObject.objects.all())
        context['aoListJS']=data
        
        data=serializers.serialize("json", Parameter.objects.all())
        context['paramListJS']=data
        
        data=serializers.serialize("json", ParameterValue.objects.all())
        context['pVListJS']=data
        

        return context
