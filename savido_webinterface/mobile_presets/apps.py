from __future__ import unicode_literals

from django.apps import AppConfig


class MobilePresetsConfig(AppConfig):
    name = 'mobile_presets'
