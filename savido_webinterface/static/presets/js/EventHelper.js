var mouse = new THREE.Vector2(); // define mouse as a vector with the size 2 (X, Y)

// mouse events
var mousedown = false;
var mousedblclick = false;

// define raycaster
var raycaster = new THREE.Raycaster();
var ray = new THREE.Ray();

// define start view angles
var alpha=90;
var beta=90;


// for DBL-Click Event (new Pos)
var newPosName;
var newPosValue = [0,0,0];

// if mouse hits object, object is selceted
var isObjectSelect = false;

// handle one mouse click
function handleMouseClick(event) {  
    // normalize mouse coordinates              
    mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
    mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;

    // create raycaster between mouse click and camera view
    raycaster.setFromCamera( mouse, camera );

    // get number of object, which the mouse intersects
    var intersects = raycaster.intersectObjects( objects );       
    // if intersects value > 0, ray hit 1 or more objects
    if ( intersects.length > 0) {
        console.log("Click Eventt");
        // get first intersect object
        var intersect = intersects[0];
        // if object is not the same as last time
        if (audioobject != intersect.object){
          if (audioobject != null){
            // if an object was click before, make it transparent
            audioobject.material.transparent = true; 
          }     
          // save intersect object in audioobject                    
          audioobject = intersect.object;
          // mouse find an object
          isObjectSelect = true;
          // make audioobject not transparent
          audioobject.material.transparent = false;

          // find the three.js audioobject in DB list 
          // and set primary key
          var whichSG, ao;
          for (var i in pVList){
                if(pVList[i].fields.valuex == audioobject.position.x && 
                    pVList[i].fields.valuez == audioobject.position.y &&
                    pVList[i].fields.valuey == audioobject.position.z){
                  console.log("Intersect");
                  var parameter = pVList[i].fields.parameter;
                  for(var j in paramList){
                    if(paramList[j].pk == parameter){
                      aoPK=paramList[j].fields.audioobject;
                      for (var k in aoList){
                        if(aoList[k].pk == aoPK){
                          objPK=aoList[k].fields.obj;
                          showAO(); // open dat.GUI - Window
                        }
                      }
                    }
                  }
                }
          }        
        }
    }
    else{
      // if no object was find with the raycaster
      //make all objects transparent
      makeAllObjectsTransparent();
    if(gui)
      gui.destroy(); // destroy dat.GUI - window
    }
}

// handle double mouse click
function handleDblClick(event) {
    // if another dat.GUI - Window is open, destroy
    if (gui)
      gui.destroy();
    // create new dat.GUI window
    gui = new dat.GUI();

    // normalize mouse coordinates
    mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
    mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;  
    // set raycaster    
    raycaster.setFromCamera( mouse, camera );
    //create new audioobect sphere for a new postion
    var materialObject = new THREE.MeshBasicMaterial({transparent: false, opacity: 0.5});
    audioobject = new THREE.Mesh(geometryObject, materialObject);
    // audioobject get position from raycaster
    audioobject.position.x=raycaster.ray.direction.x;
    audioobject.position.y=raycaster.ray.direction.y;
    audioobject.position.z=raycaster.ray.direction.z;
    // add new position to scene
    scene.add(audioobject);
    // push audioobject in object array
    objects.push(audioobject);

    // add pos name text area to dat.GUI window
    var PosName=function(){
      this.name="";
    };

    var posName = new PosName();

    var name=gui.add(posName, 'name').name("Position Name");

    // add pos value int areas to dat.GUI window
    var x = gui.add(audioobject.position, 'x', -14, 14).listen().name("X");    
    var z = gui.add(audioobject.position, 'z', -14, 14).listen().name("Y");
    var y = gui.add(audioobject.position, 'y', -14, 14).listen().name("Z");
   
    // on Change: save value in newPosValue-array
    x.onChange(function(value){
      newPosValue[0]=value;
    });
    y.onChange(function(value){
      newPosValue[1]=value;
    });
    z.onChange(function(value){
      newPosValue[2]=value;
    });
        
    // create select down menu
    var CreateNew = function(){
      this.choice = "addNewAO";
    }
    var createNew = new CreateNew();
    var options=[];
    // create options with allready existing audioobjects
    for (i in aoList)
      options.push(aoList[i].fields.name);
    // and an option to create an all new audioobject
    options.push("new AudioObject");
    // add select menu
    var selectMenu = gui.add(createNew, 'choice', options).name("New Pos in");
    selectMenu.onChange(function(choice){
      newPosName= posName['name'];
      var button = new Button();
      if (choice == "new AudioObject")
        aoPK = -1;      
      else{
        for (var i in aoList){
          if(aoList[i].fields.name == choice)
              aoPK = aoList[i].pk;                
        }
      }
      var nextButton = gui.add(button, 'showAO').name("Next"); // go to the next window
    });
    
}

var distanceObj, directionX, directionY;

// if mouse is down, set mousedown true
function handleMouseDown(event) {
  mousedown=true;   
}

// if mouse goes up, set mousedown false
function handleMouseUp(event) {
    mousedown=false;  
}

// if mouse is moving
function handleMouseMove(event) {
    directionX, directionY = 0;
    // if mouse find an object
    if (mousedown == true && isObjectSelect == true){
      var oldX = audioobject.position.x;
      var oldZ = audioobject.position.z;
      var oldDistance = Math.sqrt(oldX*oldX + oldZ*oldZ);
      distanceObj = oldDistance;

      mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
      mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;

      calcPos();
    }else if(isObjectSelect == false && mousedown == true){
      // else change camera view
      var oldX = mouse.x;
      var oldY = mouse.y;
      
      mouse.x = (event.clientX - canvas.offsetLeft) / canvas.clientWidth * 2 - 1;
      mouse.y = -((event.clientY - canvas.offsetTop) / canvas.clientHeight) * 2 + 1;
     
      if (oldX > mouse.x)
        beta = beta + 1;
      else
        beta = beta - 1;

      loadScenePosition(alpha, beta);


    }
}

/*function handleMouseWheel(event){
      
    if (isObjectSelect == true){
      var oldX = audioobject.position.x;
      var oldZ = audioobject.position.z;
      var oldDistance = Math.sqrt(oldX*oldX + oldZ*oldZ);
      distanceObj = oldDistance - event.wheelDelta;

      calcPos();

  }
}*/

function calcPos(){
   // calculation of audiooobject position depands on camera angle
   if (audioobject && distanceObj){

     alphaObj = Math.asin(mouse.x/distanceObj);
     betaObj = Math.asin(mouse.y/distanceObj);

     audioobject.position.x = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj)) * Math.cos(degToRad(beta - 90 - betaObj));
     audioobject.position.z = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj)) * Math.sin(degToRad(beta - 90 - betaObj));
     audioobject.position.y = distanceObj * Math.sin(degToRad(alpha - 90 - alphaObj));

     console.log(distanceObj);
     console.log(audioobject.position);
   }


}

// Key event
function handleKeyDown(event) {
    if(event.which==83) // S
      alpha=alpha+1;        
    if(event.which==87) // W
      alpha=alpha-1;
    if(event.which==68) // D
      beta=beta+1;
    if(event.which==65) // A
      beta=beta-1;
/*    if(event.which==107)
      audioobject.position.z +=0.5;
    if(event.which==109)
      audioobject.position.z -=0.5; */
    // load new camera view
    loadScenePosition(alpha, beta);
    
  }
