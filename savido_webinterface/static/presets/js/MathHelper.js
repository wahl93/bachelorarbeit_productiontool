  // Degree to radiant
  function degToRad(degrees) {
    return degrees * Math.PI / 180;
  }
